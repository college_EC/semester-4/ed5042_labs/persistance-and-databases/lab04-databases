package ie.ul.android.lab_week5;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

public class BelgianBeersDB {

    /*********************
     * Definition of table columns
     ***********************************************/
    //The index (key) column name for use in where clauses.
    public static final String KEY_ID = "_id";

    //The name and column index of each column in your database.
    //These should be descriptive.
    public static final String KEY_BEER_NAME =
            "beer_name";
    public static final String KEY_ALCOHOL_PERCENTAGE =
            "alcohol_percentage";
    public static final String KEY_PRICE =
            "price";

    public static final String KEY_VOLUME = "volume";

    private Context context;

    // Database open/upgrade helper
    private ModuleDBOpenHelper moduleDBOpenHelper;

    /************************
     * Constructor
     ***************************************************************/
    public BelgianBeersDB(Context context) {
        this.context = context;
        moduleDBOpenHelper = new ModuleDBOpenHelper(context, ModuleDBOpenHelper.DATABASE_NAME, null,
                ModuleDBOpenHelper.DATABASE_VERSION);

        // populate the database with some data in case it is empty
        if (getAll().length == 0) {
            this.addRow("La Chouffe", 6.0f, 2.3f, 500);
            this.addRow("La Trappe", 5.0f, 2.4f, 500);
            this.addRow("Delirium Tremens", 9.0f, 3.5f, 500);
            this.addRow("Duvel", 6.3f, 2.6f, 500);
            this.addRow("Rochefort Nr10", 7.1f, 3.4f, 500);
            this.addRow("Rochefort Nr12", 8.1f, 5.0f, 500);
            this.addRow("Affligem", 4.5f, 1.2f, 500);
            this.addRow("Kriek", 3.5f, 1.39f, 500);
            this.addRow("Bush Beer", 13f, 5.3f, 500);
            this.addRow("Mort Subite", 6.0f, 2.3f, 800);
            this.addRow("Agnus Dei", 4.3f, 3.56f, 500);
        }
    }

    /************************
     * Standard Database methods
     *************************************************/

    // Called when you no longer need access to the database.
    public void closeDatabase() {
        moduleDBOpenHelper.close();
    }

    public void addRow(String beerName, float alcoholPercentage, float price, int volume) {
        // Create a new row of values to insert.
        ContentValues newValues = new ContentValues();

        // Assign values for each row.
        newValues.put(KEY_BEER_NAME, beerName);
        newValues.put(KEY_ALCOHOL_PERCENTAGE, alcoholPercentage);
        newValues.put(KEY_PRICE, price);
        newValues.put(KEY_VOLUME, volume);

        // Insert the row into your table
        SQLiteDatabase db = moduleDBOpenHelper.getWritableDatabase();
        db.insert(ModuleDBOpenHelper.DATABASE_TABLE, null, newValues);
    }

    public void deleteRow(int idNr) {
        // Specify a where clause that determines which row(s) to delete.
        // Specify where arguments as necessary.
        String where = KEY_ID + "=" + idNr;
        String whereArgs[] = null;

        // Delete the rows that match the where clause.
        SQLiteDatabase db = moduleDBOpenHelper.getWritableDatabase();
        db.delete(ModuleDBOpenHelper.DATABASE_TABLE, where, whereArgs);
    }

    public void deleteAll() {
        String where = null;
        String whereArgs[] = null;

        // Delete the rows that match the where clause.
        SQLiteDatabase db = moduleDBOpenHelper.getWritableDatabase();
        db.delete(ModuleDBOpenHelper.DATABASE_TABLE, where, whereArgs);
    }

    /************************
     * User specific database queries
     *******************************************/
   
  /*
   * Obtain all database entries and return as human readable content in a String array
   * A query with all fields set to null will result in the whole database being returned
   * The following SQL query is implemented: SELECT * FROM  Beers
   */
    public String[] getAll() {

        ArrayList<String> outputArray = new ArrayList<String>();
        String[] result_columns = new String[]{
                KEY_BEER_NAME, KEY_ALCOHOL_PERCENTAGE, KEY_PRICE, KEY_VOLUME};

        String beerName;
        float alcoholPercentage;
        float price;
        int volume;

        String where = null;
        String whereArgs[] = null;
        String groupBy = null;
        String having = null;
        String order = null;

        SQLiteDatabase db = moduleDBOpenHelper.getWritableDatabase();
        Cursor cursor = db.query(ModuleDBOpenHelper.DATABASE_TABLE,
                result_columns, where,
                whereArgs, groupBy, having, order);
        //
        boolean result = cursor.moveToFirst();
        while (result) {
            beerName = cursor.getString(cursor.getColumnIndex(KEY_BEER_NAME));
            alcoholPercentage = cursor.getFloat(cursor.getColumnIndex(KEY_ALCOHOL_PERCENTAGE));
            price = cursor.getFloat(cursor.getColumnIndex(KEY_PRICE));
            volume = cursor.getInt(cursor.getColumnIndex(KEY_VOLUME));

            outputArray.add(beerName + " "+context.getString(R.string.cost_str_1) + " "+ alcoholPercentage + context.getString(R.string.cost_str_2) + price + " " + volume + "ml");
            result = cursor.moveToNext();

        }
        return outputArray.toArray(new String[outputArray.size()]);
    }

    /*
     * Obtain all database entries with a price lower than maxPrice and return as human readable content in a String array
     * The following SQL query is implemented: SELECT * FROM Beers WHERE price < maxPrice
     */
    public String[] getAllCheaperThan(Float maxPrice) {

        ArrayList<String> outputArray = new ArrayList<String>();
        String[] result_columns = new String[]{
                KEY_BEER_NAME, KEY_ALCOHOL_PERCENTAGE, KEY_PRICE};

        String beerName;
        float alcoholPercentage;
        float price;

        String where = KEY_PRICE + "< ?";
        String whereArgs[] = {maxPrice.toString()};
        String groupBy = null;
        String having = null;
        String order = null;

        SQLiteDatabase db = moduleDBOpenHelper.getWritableDatabase();
        Cursor cursor = db.query(ModuleDBOpenHelper.DATABASE_TABLE,
                result_columns, where,
                whereArgs, groupBy, having, order);
        //
        boolean result = cursor.moveToFirst();
        while (result) {
            beerName = cursor.getString(cursor.getColumnIndex(KEY_BEER_NAME));
            alcoholPercentage = cursor.getFloat(cursor.getColumnIndex(KEY_ALCOHOL_PERCENTAGE));
            price = cursor.getFloat(cursor.getColumnIndex(KEY_PRICE));

            outputArray.add(beerName + " "+context.getString(R.string.cost_str_1) + " "+ alcoholPercentage + context.getString(R.string.cost_str_2) + price);
            result = cursor.moveToNext();
        }
        return outputArray.toArray(new String[outputArray.size()]);
    }
 
   
  /*
   * Obtain all database entries with an alcohol percentage higher than minPercentage and return as human readable content in a String array
   * The following SQL query is implemented: SELECT * FROM Beers WHERE alcohol_percentage > minPercentage
   */


    public String[] getAllStrongerThan(Float minPercentage) {
        ArrayList<String> result = new ArrayList<>();
        String beerName;
        String[] resultColumns = {this.KEY_BEER_NAME, this.KEY_ALCOHOL_PERCENTAGE,this.KEY_PRICE};
        String where = this.KEY_ALCOHOL_PERCENTAGE + "> ?";
        String[] whereArgs = {minPercentage.toString()};
        String groupBy = null;
        String having = null;
        String order = null;
        SQLiteDatabase db = moduleDBOpenHelper.getWritableDatabase();
        Cursor cursor = db.query(ModuleDBOpenHelper.DATABASE_TABLE, resultColumns, where, whereArgs, groupBy, having, order);
        boolean dbResult = cursor.moveToFirst();
        float alcoholPercentage, price;

        while(dbResult){
            beerName = cursor.getString(cursor.getColumnIndex(KEY_BEER_NAME));
            alcoholPercentage = cursor.getFloat(cursor.getColumnIndex(KEY_ALCOHOL_PERCENTAGE));
            price = cursor.getFloat(cursor.getColumnIndex(KEY_PRICE));

            result.add(beerName + " "+context.getString(R.string.cost_str_1) + " "+ alcoholPercentage + context.getString(R.string.cost_str_2) + price);
            dbResult = cursor.moveToNext();
        }

        return result.toArray(new String[result.size()]);
    }


    /*
     * Obtain all beer names from the database and return in String[]
     */
    public String[] getBeerNames() {

        ArrayList<String> outputArray = new ArrayList<String>();
        String[] result_columns = new String[]{
                KEY_BEER_NAME};

        String beerName;

        String where = null;
        String whereArgs[] = null;
        String groupBy = null;
        String having = null;
        String order = null;

        SQLiteDatabase db = moduleDBOpenHelper.getWritableDatabase();
        Cursor cursor = db.query(ModuleDBOpenHelper.DATABASE_TABLE,
                result_columns, where,
                whereArgs, groupBy, having, order);
        //
        boolean result = cursor.moveToFirst();
        while (result) {
            beerName = cursor.getString(cursor.getColumnIndex(KEY_BEER_NAME));

            outputArray.add(beerName);
            result = cursor.moveToNext();
        }
        return outputArray.toArray(new String[outputArray.size()]);
    }

    /*
     * Return alcohol percentage of first beer found with name beerName. The following SQL query is implemented:
     * SELECT _id, alcohol_percentage FROM Beers WHERE beer_name = beerName
     */
    public float getAlcohol(String beerName) {
        String[] result_columns = new String[]{
                KEY_ID, KEY_ALCOHOL_PERCENTAGE};


        String where = KEY_BEER_NAME + "= ?";
        String whereArgs[] = {beerName};
        String groupBy = null;
        String having = null;
        String order = null;

        SQLiteDatabase db = moduleDBOpenHelper.getWritableDatabase();
        Cursor cursor = db.query(ModuleDBOpenHelper.DATABASE_TABLE,
                result_columns, where,
                whereArgs, groupBy, having, order);
        if (cursor.moveToFirst()) {
            int columnAlcoholPercentage = cursor.getColumnIndex(KEY_ALCOHOL_PERCENTAGE);
            return cursor.getFloat(columnAlcoholPercentage);
        } else return 0;
    }

    /*
     * Return price of first beer found with name beerName. The following SQL query is implemented:
     * SELECT _id, price FROM Beers WHERE beer_name = beerName
     */
    public float getPrice(String beerName) {
        String[] resultColumns = {this.KEY_ID, this.KEY_PRICE};

        String where = this.KEY_BEER_NAME + "= ?";
        String[] whereArgs = {beerName};
        String groupBy = null;
        String having = null;
        String order = null;

        SQLiteDatabase db = moduleDBOpenHelper.getWritableDatabase();
        Cursor cursor = db.query(moduleDBOpenHelper.DATABASE_TABLE, resultColumns, where, whereArgs, groupBy, having, order);

        if(cursor.moveToFirst()){
            return cursor.getFloat(cursor.getColumnIndex(this.KEY_PRICE));
        } else {
            return 0;
        }
    }

    /*
     * This is a helper class that takes a lot of the hassle out of using databases. Use as is and complete the following as required:
     * 	- DATABASE_TABLE
     * 	- DATABASE_CREATE
     */
    private static class ModuleDBOpenHelper extends SQLiteOpenHelper {

        private static final String DATABASE_NAME = "myDatabase.db";
        private static final String DATABASE_TABLE = "Beers";
        private static final int DATABASE_VERSION = 1;

        // SQL Statement to create a new database.
        private static final String DATABASE_CREATE = "create table " +
                DATABASE_TABLE + " (" + KEY_ID +
                " integer primary key autoincrement, " +
                KEY_BEER_NAME + " text not null, " +
                KEY_ALCOHOL_PERCENTAGE + " float, " +
                KEY_PRICE + " float, " + KEY_VOLUME + " int);";


        public ModuleDBOpenHelper(Context context, String name,
                                  CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        // Called when no database exists in disk and the helper class needs
        // to create a new one.
        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DATABASE_CREATE);
        }

        // Called when there is a database version mismatch meaning that
        // the version of the database on disk needs to be upgraded to
        // the current version.
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion,
                              int newVersion) {
            // Log the version upgrade.
            Log.w("TaskDBAdapter", "Upgrading from version " +
                    oldVersion + " to " +
                    newVersion + ", which will destroy all old data");

            // Upgrade the existing database to conform to the new
            // version. Multiple previous versions can be handled by
            // comparing oldVersion and newVersion values.

            // The simplest case is to drop the old table and create a new one.
            db.execSQL("DROP TABLE IF IT EXISTS " + DATABASE_TABLE);
            // Create a new one.
            onCreate(db);
        }
    }

    public String[] getAllMoreVolumeThan(Integer minVolume) {
        ArrayList<String> result = new ArrayList<>();
        String beerName;
        String[] resultColumns = {this.KEY_BEER_NAME, this.KEY_VOLUME,this.KEY_PRICE};
        String where = this.KEY_VOLUME + "> ?";
        String[] whereArgs = {minVolume.toString()};
        String groupBy = null;
        String having = null;
        String order = null;
        SQLiteDatabase db = moduleDBOpenHelper.getWritableDatabase();
        Cursor cursor = db.query(ModuleDBOpenHelper.DATABASE_TABLE, resultColumns, where, whereArgs, groupBy, having, order);
        boolean dbResult = cursor.moveToFirst();
        float alcoholVolume, price;

        while(dbResult){
            beerName = cursor.getString(cursor.getColumnIndex(KEY_BEER_NAME));
            alcoholVolume = cursor.getInt(cursor.getColumnIndex(KEY_VOLUME));
            price = cursor.getFloat(cursor.getColumnIndex(KEY_PRICE));

            result.add(beerName + " "+context.getString(R.string.cost_str_1) + " "+ alcoholVolume + context.getString(R.string.cost_str_3) + price);
            dbResult = cursor.moveToNext();
        }

        return result.toArray(new String[result.size()]);
    }
}

